import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainHeroesComponent } from './heroes/main-heroes/main-heroes.component';
import { ListHeroesComponent } from './heroes/list-heroes/list-heroes.component';
import { ListarUsuariosComponent } from './usuarios/listar-usuarios/listar-usuarios.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'usuarios',
    pathMatch: 'full'
  },
  {
    path: 'usuarios',
    children: [
      {
        path: '',
        component: ListarUsuariosComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
