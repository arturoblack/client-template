import { Component, OnInit } from '@angular/core';
import { HeroesService } from 'src/app/services/heroes.service';

@Component({
  selector: 'app-list-heroes',
  templateUrl: './list-heroes.component.html',
  styleUrls: ['./list-heroes.component.css']
})
export class ListHeroesComponent implements OnInit {
  heroes: any;
  constructor(private heroService: HeroesService) { }

  ngOnInit() {
    this.heroService.list().subscribe(
      (data) => this.heroes = data,
      console.error,
    );
  }

}
