import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainHeroesComponent } from './heroes/main-heroes/main-heroes.component';
import { ListHeroesComponent } from './heroes/list-heroes/list-heroes.component';
import { ListarUsuariosComponent } from './usuarios/listar-usuarios/listar-usuarios.component';

@NgModule({
  declarations: [
    AppComponent,
    MainHeroesComponent,
    ListHeroesComponent,
    ListarUsuariosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
