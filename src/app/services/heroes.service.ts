import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {

  constructor(private http: HttpClient) { }

  list(): Observable<any> {
    return this.http.get('http://localhost:3000/heroes');
  }

  get(id: number): Observable<any> {
    return this.http.get(`http://localhost:3000/heroes/${id}`);
  }

  create(id: number, data: any): Observable<any> {
    return this.http.post('http://localhost:3000/heroes', data);
  }

  update(id: number, data: any): Observable<any> {
    return this.http.put(`http://localhost:3000/heroes/${id}`, data);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`http://localhost:3000/heroes/${id}`);
  }
}
